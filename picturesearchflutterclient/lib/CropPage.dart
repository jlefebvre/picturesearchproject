import 'package:flutter/material.dart';

import 'package:image_crop/image_crop.dart';

import 'dart:io';

class CropPage extends StatelessWidget {
  final File file;

  CropPage({Key key, @required this.file}) : super(key: key);

  final cropKey = GlobalKey<CropState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Crop image"),
        ),
          body: Center(
            child: Container(
              color: Colors.grey,
              padding: const EdgeInsets.all(20.0),
              child: Crop(
                key: cropKey,
                image: FileImage(file),
              ),
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Stack(children: <Widget>[
            Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                heroTag: "Validate",
                onPressed: () {
                  Navigator.pop(
                      context,
                      ImageCrop.cropImage(
                          file: this.file, area: cropKey.currentState.area));
                },
                child: Icon(Icons.check),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton(
                heroTag: "Cancel",
                onPressed: () {
                  Navigator.pop(context, "nothing");
                },
                child: Icon(Icons.clear),
              ),
            ),
          ])),
    );
  }
}
