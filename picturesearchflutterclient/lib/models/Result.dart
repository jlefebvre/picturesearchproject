class Result {
  String label;
  num score;
  String url;

  Result(String label, num score, String url){
    this.label=label;
    this.score=score;
    this.url=url;
  }

  Result.fromJson(Map json)
      : label = json['label'],
        score = json['score'],
        url = json['url'];

  Map toJson() {
    return {'label': label, 'score': score, 'url': url};
  }
}