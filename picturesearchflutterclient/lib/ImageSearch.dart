import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;

import 'ResultPage.dart';
import 'CropPage.dart';

void main() => runApp(ImageSearchPage());

class ImageSearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      home: Scaffold(
        body: Center(
          child: MyImagePicker(),
        ),
      ),
    );
  }
}

class MyImagePicker extends StatefulWidget {
  @override
  MyImagePickerState createState() => MyImagePickerState();
}

class MyImagePickerState extends State {
  File imageURI;

  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.photo_library),
                  onPressed: () async => getImageFromGallery(),
                ),
                IconButton(
                  icon: Icon(Icons.photo_camera),
                  onPressed: () async => getImageFromCamera(),
                ),
              ]),
        ),
        body: Center(
          child: imageURI == null
              ? Text('No image selected.')
              : Image.file(imageURI,
                  width: queryData.size.width,
                  height: queryData.size.height,
                  fit: BoxFit.scaleDown),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: Stack(children: <Widget>[
          Align(
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              heroTag: "Search",
              onPressed: () {
                if (imageURI == null) {
                  Fluttertoast.showToast(
                      msg: "No image",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {
                  launchPOSTSearch();
                }
              },
              tooltip: 'Search',
              child: Icon(Icons.search),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: FloatingActionButton(
              heroTag: "Crop",
              onPressed: () {
                if (imageURI == null) {
                  Fluttertoast.showToast(
                      msg: "No image",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {
                  startCropPage();
                }
              },
              tooltip: 'Crop',
              child: Icon(Icons.crop),
            ),
          )
        ]));
  }

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      imageURI = image;
    });
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      imageURI = image;
    });
  }

  // Launch POST Search method
  Future launchPOSTSearch() async {
    // Is there any image selected ?
    if (imageURI == null) {
      Fluttertoast.showToast(
          msg: "No image",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "Sending request..",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.grey,
          textColor: Colors.white,
          fontSize: 16.0);
      // Give API URI
      var postUri =
          Uri.parse("http://deep-fashion-server.herokuapp.com/img_searches/");
      // Construct request
      var request = new http.MultipartRequest("POST", postUri);
      // Add picture to request
      request.files
          .add(await http.MultipartFile.fromPath('image', imageURI.path));
      // Send request
      var res = await request.send();
      // Handle response
      http.Response.fromStream(res).then((response) {
        if (response.statusCode == 201) {
          // Object created : Success
          Fluttertoast.showToast(
              msg: "201: Created",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          // Start Result Page
          startResultPage(response.headers['location']);
        } else {
          Fluttertoast.showToast(
              msg: response.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      });
    }
  }

  void startResultPage(String location) {
    var arr = location.split('/');
    arr.removeLast();
    Fluttertoast.showToast(
        msg: "id: " + arr.last,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ResultPage(id: int.parse(arr.last))),
    );
  }

  startCropPage() async {
    Fluttertoast.showToast(
        msg: "crop page start",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 16.0);
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CropPage(file: imageURI)),
    );
    if (result == "nothing") {
      Fluttertoast.showToast(
          msg: "Crop cancelled",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.grey,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "Crop complete",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      setState(() {
        imageURI = result;
      });
    }
  }
}
