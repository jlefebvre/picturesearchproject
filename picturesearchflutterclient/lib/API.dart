import 'dart:async';
import 'package:http/http.dart' as http;

const baseUrl = "https://deep-fashion-server.herokuapp.com";

class API {
  static Future getResults(int id) {
    var url = baseUrl + "/img_searches/"+id.toString();
    return http.get(url);
  }
}