import 'package:flutter/material.dart';
import 'models/Result.dart';
import 'dart:convert';

import 'API.dart';

class ResultPage extends StatelessWidget {
  final int id;

  ResultPage({Key key, @required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Result page',
      home: MyListScreen(id: this.id),
    );
  }
}

class MyListScreen extends StatefulWidget {
  final int id;

  MyListScreen({Key key, @required this.id}) : super(key: key);

  @override
  createState() => _MyListScreenState(id: this.id);
}

class _MyListScreenState extends State {
  var results = new List<Result>();

  final int id;

  _MyListScreenState({@required this.id}) : super();

  getResults(int id) {
    API.getResults(id).then((response) {
      setState(() {
        Iterable list = json.decode(response.body)['results'];
        results = list.map((model) => Result.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    getResults(this.id);
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Search results"),
        ),
        body: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
          ),
          itemCount: results.length,
          itemBuilder: (context, index) {
            return GridTile(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Image.network(
                        'https://raw.githubusercontent.com/aryapei/In-shop-Clothes-From-Deepfashion/master/' +
                            results[index].url,
                        fit: BoxFit.fitHeight,
                      ),
                      height: 160),
                  Text(results[index].label, style: TextStyle(fontWeight: FontWeight.bold),),
                  Text((results[index].score * 100).toStringAsFixed(2) + "%",style: TextStyle(fontStyle: FontStyle.italic),),
              ],
            ));
          },
        )
    );
  }
}
